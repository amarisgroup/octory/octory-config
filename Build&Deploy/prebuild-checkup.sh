#!/bin/bash

# Remove the Admin section if found with Scout to avoid the end-user using the Admin menu
# and validate the configuration with the config command-line tool.
#
# [Scout]: https://github.com/ABridoux/scout
# [config]: https://gitlab.com/amarisgroup/octory-config

# Arguments
file=$1

# remove admin section if present
if admin=$(scout read "Admin" -i $file 2>/dev/null); then
	echo "➡️ Removing Admin section"
	scout delete "Admin" -m $file
fi

# check configuration
config check $1