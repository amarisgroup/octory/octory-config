# Welcoming curtain

A single slide with up and down banner images. Also, a (really nice) logo under the welcoming text. All the images will change depending on the current mode: dark or light, with the keys `LightModeURL` and `DarkModeURL`.

The size of the window is fixed with the keys `MinimumSize` and `MaximumSize` set to the same fixed value. Also, the key `IsResizable` is set to "false".

You can freely use all the images in the resources, or use yours. You can take the ratio of the images in the resources as a guide.

Finally,  the *FontStyles* section contains two font styles: "Title" and "Body", which are use to format the welcoming text.

![Light](Light.png)

![Dark](Dark.png)

#### Author

Alexis Bridoux
Octory Lead developer & Product owner

[Email](mailto:alexis.bridoux@amaris.com)

[Github](https://github.com/ABridoux)