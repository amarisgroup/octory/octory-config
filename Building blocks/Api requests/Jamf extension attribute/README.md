# Api request - Jamf extension attribute 

This request will put a value `#Value#` inside a Jamf extension attribute, using the classic Jamf API.
To use it, add a `SendRequest` action inside an [ActionSet](https://documents.getceremony.app/Actions.html#action-sets), which will for example be executed when the application is launched or when the user clicks a button.

The plist file is given as an example to show you how the Api request can be used.

### To be filled:
- Jamf API URL
- Computer Id
- Extension attribute id
- Value

#### Xml

```xml
<key>JamfBaseURL</key>
<string>https://#JAMF_API_URL#/JSSResource/</string>
<key>Models</key>
<dict>
		<key>Endpoint</key>
		<string>computers/id/#COMPUTER_ID#</string>
		<key>MDMApi</key>
		<string>Jamf</string>
		<key>Method</key>
		<string>PUT</string>
		<key>Body</key>
		<dict>
			<key>computer</key>
			<dict>
				<key>extension_attributes</key>
				<dict>
					<key>extension_attribute</key>
					<dict>
						<key>id</key>
						<integer>#EXTENSION_ATTRIBUTE_ID#</integer>
						<key>value</key>
						<string>#VALUE#</string>
					</dict>
				</dict>
			</dict>
		</dict>
</dict>
```
#### Plist Editor

The extension attribute integer value is set to 0 to let you copy and paste into Plist Editor.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<key>JamfBaseURL</key>
<string>https://#JAMF_API_URL#/JSSResource/</string>
<key>Models</key>
<dict>
		<key>Endpoint</key>
		<string>computers/id/#COMPUTER_ID#</string>
		<key>MDMApi</key>
		<string>Jamf</string>
		<key>Method</key>
		<string>PUT</string>
		<key>Body</key>
		<dict>
			<key>computer</key>
			<dict>
				<key>extension_attributes</key>
				<dict>
					<key>extension_attribute</key>
					<dict>
						<key>id</key>
						<integer>0</integer>
						<key>value</key>
						<string>#VALUE#</string>
					</dict>
				</dict>
			</dict>
		</dict>
</dict>
</plist>
```